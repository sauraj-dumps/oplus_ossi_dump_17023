#!/bin/bash

cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat my_preload/del-app/amazonshopping_in/in.amazon.mShop.android.shopping.apk.* 2>/dev/null >> my_preload/del-app/amazonshopping_in/in.amazon.mShop.android.shopping.apk
rm -f my_preload/del-app/amazonshopping_in/in.amazon.mShop.android.shopping.apk.* 2>/dev/null
cat my_preload/del-app/snapchat/com.snapchat.android.apk.* 2>/dev/null >> my_preload/del-app/snapchat/com.snapchat.android.apk
rm -f my_preload/del-app/snapchat/com.snapchat.android.apk.* 2>/dev/null
cat my_preload/del-app/Sharechat/in.mohalla.sharechat.apk.* 2>/dev/null >> my_preload/del-app/Sharechat/in.mohalla.sharechat.apk
rm -f my_preload/del-app/Sharechat/in.mohalla.sharechat.apk.* 2>/dev/null
cat my_preload/del-app/flipkart/com.flipkart.android.apk.* 2>/dev/null >> my_preload/del-app/flipkart/com.flipkart.android.apk
rm -f my_preload/del-app/flipkart/com.flipkart.android.apk.* 2>/dev/null
cat my_preload/del-app/moj/in.mohalla.video.apk.* 2>/dev/null >> my_preload/del-app/moj/in.mohalla.video.apk
rm -f my_preload/del-app/moj/in.mohalla.video.apk.* 2>/dev/null
cat my_preload/del-app/spotify_in/com.spotify.music.apk.* 2>/dev/null >> my_preload/del-app/spotify_in/com.spotify.music.apk
rm -f my_preload/del-app/spotify_in/com.spotify.music.apk.* 2>/dev/null
cat my_product/app/OplusCamera/OplusCamera.apk.* 2>/dev/null >> my_product/app/OplusCamera/OplusCamera.apk
rm -f my_product/app/OplusCamera/OplusCamera.apk.* 2>/dev/null
cat system/system/apex/com.google.android.art.apex.* 2>/dev/null >> system/system/apex/com.google.android.art.apex
rm -f system/system/apex/com.google.android.art.apex.* 2>/dev/null
cat my_stock/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null >> my_stock/priv-app/OppoGallery2/OppoGallery2.apk
rm -f my_stock/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null
cat my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat my_bigball/app/Photos/Photos.apk.* 2>/dev/null >> my_bigball/app/Photos/Photos.apk
rm -f my_bigball/app/Photos/Photos.apk.* 2>/dev/null
cat my_bigball/app/Browser/Browser.apk.* 2>/dev/null >> my_bigball/app/Browser/Browser.apk
rm -f my_bigball/app/Browser/Browser.apk.* 2>/dev/null
cat my_bigball/priv-app/Messages/Messages.apk.* 2>/dev/null >> my_bigball/priv-app/Messages/Messages.apk
rm -f my_bigball/priv-app/Messages/Messages.apk.* 2>/dev/null
cat my_bigball/priv-app/KeKeUserCenter/KeKeUserCenter.apk.* 2>/dev/null >> my_bigball/priv-app/KeKeUserCenter/KeKeUserCenter.apk
rm -f my_bigball/priv-app/KeKeUserCenter/KeKeUserCenter.apk.* 2>/dev/null
cat my_bigball/del-app-pre/Duo_del/Duo.apk.* 2>/dev/null >> my_bigball/del-app-pre/Duo_del/Duo.apk
rm -f my_bigball/del-app-pre/Duo_del/Duo.apk.* 2>/dev/null
cat my_bigball/del-app-pre/Facebook/Facebook.apk.* 2>/dev/null >> my_bigball/del-app-pre/Facebook/Facebook.apk
rm -f my_bigball/del-app-pre/Facebook/Facebook.apk.* 2>/dev/null
cat odm/lib64/libstblur_capture_api.so.* 2>/dev/null >> odm/lib64/libstblur_capture_api.so
rm -f odm/lib64/libstblur_capture_api.so.* 2>/dev/null
cat system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat my_heytap/app/Maps/Maps.apk.* 2>/dev/null >> my_heytap/app/Maps/Maps.apk
rm -f my_heytap/app/Maps/Maps.apk.* 2>/dev/null
cat my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null >> my_heytap/app/YouTube/YouTube.apk
rm -f my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null
cat my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> my_heytap/app/WebViewGoogle/WebViewGoogle.apk
rm -f my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null >> my_heytap/app/Gmail2/Gmail2.apk
rm -f my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> my_heytap/priv-app/GmsCore/GmsCore.apk
rm -f my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> my_heytap/priv-app/Velvet/Velvet.apk
rm -f my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat .git/objects/pack/pack-045934475f6c552f488b1ae0180d09b6fb99b7ed.pack.* 2>/dev/null >> .git/objects/pack/pack-045934475f6c552f488b1ae0180d09b6fb99b7ed.pack
rm -f .git/objects/pack/pack-045934475f6c552f488b1ae0180d09b6fb99b7ed.pack.* 2>/dev/null
